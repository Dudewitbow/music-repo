#include "tasks.hpp"
#include "utilities.h"
#include "io.hpp"
#include <stdio.h>
#include <time.h>
#include <math.h>

//file parsing
#include <stdlib.h>
#include <string>
#include <fstream>

#include "v2/character_1.h"
#include "v2/character_2.h"
#include "v2/character_3.h"
#include "v2/character_4.h"
#include "v2/character_5.h"
#include "v2/character_6.h"

using namespace std;

QueueHandle_t songBuffer = 0; //holds pointer to data to write into stream
QueueHandle_t songTitle = 0; //holds song name
QueueHandle_t dir = 0; //holds RW/FF value
QueueHandle_t screenQueue = 0; //holds what should be displayed on screen
QueueHandle_t b2aQueue = 0; //button tells accel to send info
QueueHandle_t a2sQueue = 0; //accel tells screen to display steps

TaskHandle_t player = 0;
TaskHandle_t mp3 = 0;
TaskHandle_t parser = 0;

int gvolume = 75;
int currentSong = 0;
int choice = 0; //for screen and button task use
int screenDisplay = 0;

//set pin as an output pin
void pinDirOut(uint8_t port, uint32_t pin){
    if(port == 0){
        LPC_GPIO0->FIODIR |= (1 << pin);
    }
    else if(port == 1){
        LPC_GPIO1->FIODIR |= (1 << pin);
    }
    else if(port == 2){
        LPC_GPIO2->FIODIR |= (1 << pin);
    }
    else puts("Non Valid Port Number\n");
}

//set pin as an input pin
void pinDirIn(uint8_t port, uint32_t pin){
    if(port == 0){
        LPC_GPIO0->FIODIR |= ~(1 << pin);
    }
    else if(port == 1){
        LPC_GPIO1->FIODIR |= ~(1 << pin);
    }
    else if(port == 2){
        LPC_GPIO2->FIODIR |= ~(1 << pin);
    }
    else puts("Non Valid Port Number\n");
}

//reads pin value
int readPin(uint8_t port, uint32_t pin){
    if (port == 0) {
		return LPC_GPIO0->FIOPIN & (1 << pin);
    }
    else if(port == 1){
        return LPC_GPIO1->FIOPIN & (1 << pin);
    }
    else if(port == 2){
        return LPC_GPIO2->FIOPIN & (1 << pin);
    }
    else puts("Non Valid Port Number\n");
    return 0;
}

//VS1053 register values
const uint8_t SCI_MODE = 0x0; // 15 clk range, 14, mic line,  13 right wrong, 12 record active, 11 vs1002 native spi, 10 share spi, 9 sdi bit order, 8 dclk active edge, 7 earspeaker high, 6 stream mode, 5 sdi test, 4 ear speaker low, 3 cancel decoding current file, 2 soft reset, 1 mpeg layers, 0 differential
const uint8_t SCI_STATUS = 0x1;
const uint8_t SCI_BASS = 0x2; // 15:12 treble control, 11:8 freqlimittreble, 7:4 amplitude, 3:0 freqlimitbass
const uint8_t SCI_CLOCKF = 0x3; //15:13 clk multiplier, 12:11 allowed multiplier addition, 10:0 clock freq
const uint8_t SCI_DECODE_TIME = 0x4;
const uint8_t SCI_AUDATA = 0x5;
const uint8_t SCI_WRAM = 0x6;
const uint8_t SCI_WRAMADDR = 0x7;
const uint8_t SCI_HDAT0 = 0x8;
const uint8_t SCI_HDAT1 = 0x9;
const uint8_t SCI_AIADDR = 0xa;
const uint8_t SCI_VOL = 0xb;
const uint8_t SCI_AICTRL0 = 0xc;
const uint8_t SCI_AICTRL1 = 0xd;
const uint8_t SCI_AICTRL2 = 0xe;
const uint8_t SCI_AICTRL3 = 0xf;
const uint8_t SCI_num_registers = 0xf;

uint8_t sendWord(uint8_t out){
    LPC_SSP0->DR = out; //DR register to write in
    while(LPC_SSP0->SR & (1<<4)); //wait state
    return LPC_SSP0->DR;
}

void write_register(uint8_t _reg,uint16_t _value)  //sdi
{
  sendWord(0x2); // Write operation
  sendWord(_reg); // Which register
  sendWord(_value >> 8); // Send hi byte
  sendWord(_value & 0xff); // Send lo byte
}

void sci_write_register(uint8_t _reg, uint16_t _value)  //write into the control register
{
    clearPin(2,5); //XCS is selelected, Active low
    write_register(_reg, _value); //write reg value and value to set it with
    setPin(2,5); //XCS pin set here, High
}

void sdi_write_register(uint8_t *data, uint16_t count) //write into the data register. for mp3 purposes, reads 512 bytes of a file and plays it back
{   
    clearPin(2,6); //XDCS is selelected, Active low
    while(count--)
    {
        sendWord(*data);
        data++;
        while (readPin(0,0) == 0) vTaskDelay(1); //wait till DREQ can accept more data
    }
    setPin(2,6); //XDCS pin set here, High
}

void setVolume(uint8_t vol)  
{
    uint16_t volume; //a hex value of xx-xxH
    //high bits left channel, low bits right channel
    volume = (vol << 8) + vol;
    sci_write_register(SCI_VOL, volume);
}

uint16_t read_register(uint8_t _reg) 
{
  uint16_t result;
  sendWord(0x3); // Read operation
  sendWord(_reg); // Which register
  result = sendWord(0xff) << 8; // read high byte
  result |= sendWord(0xff); // read low byte
  return result;
}

void initPin(){
    //Turn on pins for LCD GPIO for output 4 bit + Contorl 

    //LPC_GPIO1->FIODIR |= (1 << pin); //set as ouput pin
    LPC_GPIO1->FIODIR |= (1 << 19); //P1.19      D4 pins V
    LPC_GPIO1->FIODIR |= (1 << 20); //P1.20      D5
    LPC_GPIO1->FIODIR |= (1 << 22); //P1.22      D6
    LPC_GPIO1->FIODIR |= (1 << 23); //P1.23      D7
    LPC_GPIO1->FIODIR |= (1 << 28); //P1.28      EN Enable
    LPC_GPIO1->FIODIR |= (1 << 29); //P1.29      RS Register select, Command 0, Data 1
    //pin R/W on LCD is tied to GND
}

void initLCD(){    
    //initalize the board and set to 4bit munually if internal circuits fail
    clearPin(1,29); //Set RS to 0 to allow command read in for LCD

    // send 0x03;
    clearPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// 0x0 4 bit 0000
    setPin(1,28); delay_ms(10); clearPin(1,28); //quickly enable and disable EN to pass first pint in
    clearPin(1,23); clearPin(1,22); setPin(1,20); setPin(1,19);// 0x3 4 bit 0011
    setPin(1,28); delay_ms(10); clearPin(1,28);

    //Do two more times for a total of 3
    clearPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// 0x0 4 bit 0000
    setPin(1,28); delay_ms(10); clearPin(1,28);
    clearPin(1,23); clearPin(1,22); setPin(1,20); setPin(1,19);// 0x3 4 bit 0011
    setPin(1,28); delay_ms(10); clearPin(1,28);

    clearPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// 0x0 4 bit 0000
    setPin(1,28); delay_ms(10); clearPin(1,28);
    clearPin(1,23); clearPin(1,22); setPin(1,20); setPin(1,19);// 0x3 4 bit 0011
    setPin(1,28); delay_ms(10); clearPin(1,28);

    clearPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// all pins used are now clear/voltage 0
}

void LCDsetup(){
    //set screen to then take data
    clearPin(1,29); //Set RS to 0 to allow command read in for LCD

    //send 0x02 to start 4 bit. this 4 bit can be read as 8 bit and be sent as is, once
    clearPin(1,23); clearPin(1,22); setPin(1,20); clearPin(1,19);// 0x2 4 bit 0010
    setPin(1,28); delay_ms(10); 
    clearPin(1,28);

    //input 0x28 for 4 bit nibble and two rows on screen
    clearPin(1,23); clearPin(1,22); setPin(1,20); clearPin(1,19);// 0x2 4 bit 0010
    setPin(1,28); delay_ms(10); clearPin(1,28);
    setPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// 0x8 4 bit 1000
    setPin(1,28); delay_ms(10); clearPin(1,28);

    //input 0x0E for turn on cursor and display
    clearPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// 0x0 4 bit 0000
    setPin(1,28); delay_ms(10); clearPin(1,28);
    setPin(1,23); setPin(1,22); setPin(1,20); clearPin(1,19);// 0xE 4 bit 1110
    setPin(1,28); delay_ms(10); clearPin(1,28);

    //input 0x06 for turn on cursor and display
    clearPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// 0x0 4 bit 0000
    setPin(1,28); delay_ms(10); clearPin(1,28);
    clearPin(1,23); setPin(1,22); setPin(1,20); clearPin(1,19);// 0x6 4 bit 0110
    setPin(1,28); delay_ms(10); clearPin(1,28);    

    clearPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// all pins used are now clear/voltage 0

    //you may now input chracter
}

void clearLCD(){
    //this will delete dram on LCD and all content on screen
    clearPin(1,29); //Set RS to 0 to allow command read in for LCD

    //0x01 in command will delete screen for new content 
    //---may reset curucor for top left?---
    clearPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// 0x0 4 bit 0000
    setPin(1,28); delay_ms(10); clearPin(1,28);
    clearPin(1,23); clearPin(1,22); clearPin(1,20); setPin(1,19);// 0x1 4 bit 0001
    setPin(1,28); delay_ms(10); clearPin(1,28);

    clearPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// all pins used are now clear/voltage 0
}

void nxtLine(){
    //this may change cursor to next line?
    clearPin(1,29); //Set RS to 0 to allow command read in for LCD

    //0xC0 in command will delete screen for new content 
    setPin(1,23); setPin(1,22); clearPin(1,20); clearPin(1,19);// 0xC 4 bit 1100
    setPin(1,28); delay_ms(10); clearPin(1,28);
    clearPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// 0x0 4 bit 0000
    setPin(1,28); delay_ms(10); clearPin(1,28);

    clearPin(1,23); clearPin(1,22); clearPin(1,20); clearPin(1,19);// all pins used are now clear/voltage 0
}

void reader(string read){
    int j=0;
    for(int i=0; i<read.size(); i++){
        char a;
        a=read[i];
        // printf("%c",a);
        // printf("%d ",i );
        // printf("%d ",read.size() );


        switch (a){

        case 'A': bit_A(); break;
        case 'B': bit_B(); break;
        case 'C': bit_C(); break;
        case 'D': bit_D(); break;
        case 'E': bit_E(); break;
        case 'F': bit_F(); break;
        case 'G': bit_G(); break;    
        case 'H': bit_H(); break;
        case 'I': bit_I(); break;
        case 'J': bit_J(); break;
        case 'K': bit_K(); break;
        case 'L': bit_L(); break;
        case 'M': bit_M(); break;
        case 'N': bit_N(); break;
        case 'O': bit_O(); break;
        case 'P': bit_P(); break;
        case 'Q': bit_Q(); break;
        case 'R': bit_R(); break;
        case 'S': bit_S(); break;
        case 'T': bit_T(); break;
        case 'U': bit_U(); break;
        case 'V': bit_V(); break;
        case 'W': bit_W(); break;
        case 'X': bit_X(); break;
        case 'Y': bit_Y(); break;
        case 'Z': bit_Z(); break;

        case 'a': bit_a(); break;
        case 'b': bit_b(); break;
        case 'c': bit_c(); break;
        case 'd': bit_d(); break;
        case 'e': bit_e(); break;
        case 'f': bit_f(); break;
        case 'g': bit_g(); break;
        case 'h': bit_h(); break;
        case 'i': bit_i(); break;
        case 'j': bit_j(); break;
        case 'k': bit_k(); break;
        case 'l': bit_l(); break;
        case 'm': bit_m(); break;
        case 'n': bit_n(); break;
        case 'o': bit_o(); break;
        case 'p': bit_p(); break;
        case 'q': bit_q(); break;
        case 'r': bit_r(); break;
        case 's': bit_s(); break;
        case 't': bit_t(); break;
        case 'u': bit_u(); break;
        case 'v': bit_v(); break;
        case 'w': bit_w(); break;
        case 'x': bit_x(); break;
        case 'y': bit_y(); break;
        case 'z': bit_z(); break;

        case '0': bit_0(); break;
        case '1': bit_1(); break;
        case '2': bit_2(); break;
        case '3': bit_3(); break;
        case '4': bit_4(); break;
        case '5': bit_5(); break;
        case '6': bit_6(); break;
        case '7': bit_7(); break;
        case '8': bit_8(); break;
        case '9': bit_9(); break;

        case ' ': bit_space(); break;
        case '!': bit_exc(); break;
        //case '"': bit_dquote(); break;
        case '#': bit_pound(); break;
        case '$': bit_dollar(); break;     
        case '%': bit_percent(); break;
        case '&': bit_and1(); break;
        //case ''': bit_apost(); break;
        case '(': bit_lrbracket(); break;
        case ')': bit_rrbracket(); break;
        case '*': bit_star(); break;
        case '+': bit_plus(); break;
        case ',': bit_comma(); break;
        case '-': bit_minus(); break;
        case '.': bit_period(); break;
        case '/': bit_dash(); break;

        case ':': bit_colon(); break;
        case ';': bit_semi(); break;
        case '<': bit_lshift(); break;
        case '=': bit_equal(); break;
        case '>': bit_rshift(); break;
        case '?': bit_question(); break;
        
        default: printf("error at char number %d\n",i); break;
        }

        j++;
        if(j==15){i=read.size();}
        // if(j==15){nxtLine();}
        // if(j==31){i=read.size();}
    }
    nxtLine();
}



struct TAGdata //stores metadata and songname
{
        char tag[3];
        char title[30];
        char artist[30];
        char album[30];
        char year[4];
        char description[30];
        char genre;
};

TAGdata tagArr [100];
string tagArrName[100];
int tagArrSize = 0;

void init(){
    //pg 63
    LPC_SC->PCONP |= (1 << 21);     // The SSP 1 interface power/clock control bit.
    LPC_SC->PCLKSEL1 &= ~(3 << 10); // Clear clock Bits
    LPC_SC->PCLKSEL1 |=  (1 << 10); // 11:10 PCLK_SSP0 Peripheral clock selection for SSP1. sets base clock to system clocks level

    //P0.1 is used as ssel
    pinDirOut(0,1);
    setPin(0,1);

    //P0.0 as DREQ
    pinDirIn(0,0);

    //P2.6 is used as XDCS
    pinDirOut(2,6);
    setPin(2,6);

    //P2.5 is used as XCS
    pinDirOut(2,5);
    setPin(2,5);

    //P2.7 as reset
    pinDirOut(2,7);

    //master slave input testing page 117 of manual
    //P0.16 P0.17 P0.18 ssel miso mosi
    LPC_PINCON->PINSEL1 &= ~( (3 << 0) | (3 << 2) | (3 << 4) ); //clears functionality of the pins
    LPC_PINCON->PINSEL1 |=  ( (2 << 0) | (2 << 2) | (2 << 4) ); //SCK0 MISO0 MOSI0 in that order

    //page 422 for table values
    LPC_SSP0->CR0 = 0;          //clear CR0
    LPC_SSP0->CR0 = 0x07;       // 8 bit transfer
    LPC_SSP0->CR1 = (1 << 1);   // Control Register 1. Selects master/slave and other modes.

    //pclksel
     LPC_SC->PCLKSEL0 &= ~(3 << 16); // 48MHz / 4 = 12Mhz
     LPC_SC->PCLKSEL0 |= (1 << 16); // 48mhz
    
    //cpsdvsr
    LPC_SSP0->CPSR &= ~( (0xFF << 0)  ) ; // cpsdvsr
    LPC_SSP0->CPSR = 8;         // Clock Prescale Register SCK speed = CPU / 2

    //SCR
    LPC_SSP0->CR0 &=  ~( (0xFF << 8)  ) ;          //  SCR value clear Clock, register from 8-15
    LPC_SSP0->CR0 |=  ( (0x02 << 8)  ) ;          //set Clock speed
    //frequency = PCLK / (CPSDVSR * [SCR+1]). the default values keep it under 33MHz
}

void init_mp3()
{
    clearPin(2,7);
    setPin(2,7);
    sci_write_register(SCI_MODE, 0x0810); //setups what board does
    sci_write_register(SCI_BASS, 0x5A00); //sets arbitrary treble levels
    sci_write_register(SCI_AUDATA, 0xAC45); //stereo data
    sci_write_register(SCI_CLOCKF, 0xE3E8); //clk
}

void songParser(void *p){
    vTaskSuspend(player); //start program with the player and mp3 task disabled
    vTaskSuspend(mp3);

    char nameBuf[30];
    ifstream text;
    TAGdata songInfo;
    string line = "1:songlist.txt";
    text.open(line.c_str());
    
    while(!text.eof()){
        getline(text, line);
        sprintf(nameBuf, "1:%s", line.c_str());
        FILE* songFile = fopen(nameBuf, "r");
        if(!songFile) puts("file not opened");


        fseek(songFile, 0, SEEK_END); //moves pointer to end of file
        long pos = ftell(songFile); //save the end pointer
        fseek (songFile, (pos - 128), SEEK_SET); //moves song file pointer to 128 bytes before end of the song(ID3 Data)
        if(fread(&songInfo, sizeof(TAGdata), 1, songFile) != 1) puts( "unable to read");//does the parsing
        tagArrName[tagArrSize] = nameBuf;
        tagArr[tagArrSize] = songInfo;
        tagArrSize++;
        fclose(songFile);
    }

    srand (time(NULL));
    currentSong = rand() % tagArrSize;

    text.close();    
    while(1){
        vTaskResume(mp3);
        vTaskSuspend(parser);
    }
}

void screenTask(void* p){
    int select;
    int steps;
    while(1){
        if(xQueueReceive(screenQueue, &select, 0)){
            clearLCD();
            switch(select){
                case 0: //music playing mode
                    //printf("Title: %s\nArtist: %s\n", tagArr[currentSong].title, tagArr[currentSong].artist);
                    reader(tagArr[currentSong].title);
                    reader(tagArr[currentSong].artist);
                    break;
                case 1: //is a song file;
                    //puts("case 1");
                    //printf("%d\n%s\n", currentSong,tagArr[choice].title);
                    //printf("Title: %s\nArtist: %s\n", tagArr[choice].title, tagArr[choice].artist);
                    reader(tagArr[choice].title);
                    reader(tagArr[choice].artist);
                    break;
                case 2: //display pedometer
                    reader("Display");
                    reader("Pedometer");
                    break;
                case 3: //display pedometer value
                    xQueueReceive(a2sQueue, &steps, 0);
                    reader("Total Steps:");
                    reader(to_string(steps));
                    break;
            }
        }
        else{
            vTaskDelay(10);
        }
    }
    
}

void buttonTask(void *p){
    // p.1. 9 10 14 15
    bool playing, menu;
    bool display = 0;
    int pedometer = 2;
    int pedometer2 = 3;
    int dispMenu = 1; //what to send if its on menu screen
    playing = 0;
    menu = 1;
    bool rw = 0;
    bool ff = 1;
    bool state = 1; 
    

    while(1){
        while(playing){
            vTaskDelay(30);
            if(readPin(1,9)){
                vTaskDelay(200);
                if(readPin(1,9)){//rewind
                    while(readPin(1,9)){
                    xQueueSend(dir, &rw, 0);
                    vTaskDelay(200);
                    }
                }
                else{//prev song
                    if(currentSong > 0){
                        currentSong--;
                        xQueueSend(songTitle, &tagArrName[currentSong], 0);
                    }
                    else{
                        currentSong = tagArrSize - 1;
                        xQueueSend(songTitle, &tagArrName[currentSong], 0);
                    }
                }
            }
            if(readPin(1,10)){
                vTaskDelay(200);
                if(readPin(1, 10)){// fast foward
                    while(readPin(1, 10)){
                    xQueueSend(dir, &ff, 0);
                    vTaskDelay(200);
                    }
                }
                else{//next song
                    if(currentSong < tagArrSize - 1){
                        currentSong++;
                        xQueueSend(songTitle, &tagArrName[currentSong], 0);
                    }
                    else{
                        currentSong = 0;
                        xQueueSend(songTitle, &tagArrName[currentSong], 0);
                    }
                }
            }
            if(readPin(1,14) && state){ //pause
                vTaskSuspend(player);
                vTaskDelay(200);
                state = 0;
            }
            if(readPin(1,14) && !state){ //play
                vTaskResume(player);
                vTaskDelay(200);
                state = 1;
            }
            while(readPin(1, 15))
            {
                if(readPin(1,9)){//vol down
                    if(gvolume < 250) gvolume+=5;
                    setVolume(gvolume);
                    vTaskDelay(200);
                }
                if(readPin(1,10)){//vol up
                    if(gvolume > 5) gvolume-=5;
                    setVolume(gvolume);
                    vTaskDelay(200);
                }
                if(readPin(1,14)){//menu switch
                    playing = 0;
                    menu = 1;
                    vTaskDelay(200);
                }
            }
        }

        while(menu){
            vTaskDelay(200);
            if(display == 0){ //display current song once
                xQueueSend(screenQueue, &screenDisplay, 0);
                display = 1;
            }
            
            if(readPin(1,9)){ //move option left
                if(choice > 0) choice--;
                xQueueSend(screenQueue, &dispMenu, 0);
                vTaskDelay(200);
            }
            if(readPin(1,10)){ //move option right
                if(choice < tagArrSize) choice++;
                if(choice == tagArrSize) xQueueSend(screenQueue, &pedometer, 0);
                else xQueueSend(screenQueue, &dispMenu, 0);
                vTaskDelay(200);
            }
            if(readPin(1,14)){//select choice
                if(choice >= 0 && choice < tagArrSize) { //within song size, play song
                    currentSong = choice;
                    xQueueSend(songTitle, &tagArrName[choice], 0);
                    xQueueSend(screenQueue, &dispMenu, 0);
                    vTaskResume(mp3);
                }
                else if(choice == tagArrSize){//pedometer
                    xQueueSend(b2aQueue, &choice, 0);
                    xQueueSend(screenQueue, &pedometer2, 0);
                }

                vTaskDelay(200);
            }
            while(readPin(1, 15)){//menu switch
                if(readPin(1,14)){
                    vTaskDelay(200);
                    playing = 1;
                    menu = 0;
                    display = 0;
                }
            }
        }

        
    }
}

void playerTask(void* p)
{   
    uint8_t* buf;
    int16_t bufSize = 512;
    while(1){
        xQueueReceive(songBuffer, &buf, portMAX_DELAY);
        vTaskSuspend(mp3);
        sdi_write_register(buf, bufSize);
        vTaskResume(mp3);
    }
}

void pedometerTask(void* p)
{   
    int s;
    char buf[100];
    int x,y,z;
    int count = 0;
    double magnitude = 1;
    while(1){
        x = AS.getX() / 10;
        y = AS.getY() / 10;
        z = AS.getZ() / 10;
        magnitude = sqrt((x*x)+(y*y)+(z*z));
        if(magnitude > 200){
            count++;
            vTaskDelay(250);
        }

        if(xQueueReceive(b2aQueue, &s, 0)){
            xQueueSend(a2sQueue, &count, 0);
        }
        vTaskDelay(50);
    }
}

//it is CLKI/4. According to the Adafruit schematics, the VS1053B uses a 12.288 MHz clock. To be safe, I use a maximum clock of 3 MHz both for the SD card and the VS1053B:
//P0.0 = DREQ P0.1 = XDCS P2.7 = reset
//10 is write data, 11 is read data
void mp3Task(void* p)
{   
    uint8_t songBuff[512];
    int bufferSize = 512;
    uint8_t* songPointer = songBuff;
    string name;
    long pos;
    bool direction = 0;
    FILE *songFile;
    int read = 0;
    int sq = 0; //if the song changes change the display

    init();
    clearPin(2,7);
    setPin(2,7);
    init_mp3();
    clearPin(2,7);
    setPin(2,7);
    setVolume(gvolume);
    setVolume(gvolume);

   while(1){
        
        //xQueueReceive(songTitle, &name, 0);
        songFile = fopen(tagArrName[currentSong].c_str(), "r"); //opens a song by default

        while(fread(songBuff, sizeof(char), bufferSize, songFile) > 0){ //reads 512 bytes, breaks if reads nothing
            read =+ bufferSize;
            vTaskResume(player);//activate the player task
            pos = ftell(songFile); //save current position of read
            xQueueSend(songBuffer, &songPointer, portMAX_DELAY); //send pointer to player
            vTaskSuspend(player); //suspend player

            if(xQueueReceive(dir, &direction, 0)){//handles rewinding and fast foward
                if(direction == 0 && read >= 16384){
                    vTaskSuspend(player); //suspend player
                    fseek (songFile, (pos - 16384), SEEK_SET); //rewind 2048 bytes
                    pos = ftell(songFile);
                }
                else{
                    vTaskSuspend(player); //suspend player
                    fseek (songFile, 8192, SEEK_CUR); //fast foward 2048 bytes
                    pos = ftell(songFile);
                }
            }

            if(xQueueReceive(songTitle, &name, 0)){ //used to change a song mid playback
                vTaskSuspend(player); //suspend player
                fclose(songFile); //close opened file
                songFile = fopen(name.c_str(), "r"); //open the new file
            }
        }

        fclose(songFile);

        if(currentSong < tagArrSize - 1) currentSong++;
        else currentSong = 0;
        read = 0;
        if(screenDisplay == 0) xQueueSend(screenQueue, &sq, 0);
        vTaskDelay(1);
   }
}



int main(void)
{
    songBuffer = xQueueCreate(1, sizeof(char*));
    songTitle = xQueueCreate(1, sizeof(string));
    dir = xQueueCreate(3, sizeof(bool));
    screenQueue = xQueueCreate(1, sizeof(int));
    b2aQueue = xQueueCreate(1, sizeof(int)); 
    a2sQueue = xQueueCreate(1, sizeof(int)); 

    initPin();
    initLCD();
    LCDsetup();
    clearLCD(); //this always needs to be done upon start up

    //scheduler_add_task(new terminalTask(PRIORITY_HIGH));
    xTaskCreate(screenTask, (char*) "screenTask", STACK_BYTES(2048), 0, 1, 0);
    xTaskCreate(pedometerTask, (char*) "pedometer", STACK_BYTES(2048), 0, 2, 0);
    xTaskCreate(songParser, (char*) "songParser", STACK_BYTES(2048), 0, 3, &parser);
    xTaskCreate(buttonTask, (char*) "buttonTask", STACK_BYTES(2048), 0, 2, 0);
    xTaskCreate(mp3Task, (char*) "mp3Task", STACK_BYTES(4096), 0, 2, &mp3);
    xTaskCreate(playerTask, (char*) "playerTask", STACK_BYTES(2048), 0, 1, &player);
    //scheduler_start(); ///< This shouldn't return
    vTaskStartScheduler();
    return -1;
}
